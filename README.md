# Timesheet

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

First step of this setup is to install *yarn* .
Follow installation process [here](https://yarnpkg.com/lang/en/docs/install) and check if you were successful with:
```
yarn --version
```

If you don't have *Node.js* installed make sure to follow this [link](https://nodejs.org/en/).

Relevant packages needed to successfully deploy application are found in ```package.json``` and are listed here:

- moment
- react
- react-dom
- react-router-dom
- react-scripts

Run 
```
yarn build 
``` 
to build the app. If any of the dependencies are missing run
```
yarn install
```
to install those packages.

## Development Server

Running
 ```
  yarn start 
 ```
starts development server. Open URL (by default *localhost:3000*) to view it in browser.
Hot reloading is enabled so if you make any changes to the app they will be reflected automatically.