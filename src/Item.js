import React from "react";

function Item(props) {
  let style = {
    textAlign: "center"
  };

  return (
    <div className="item-row">
      <div className="check-flag">
        <span className="small-text-label">Title</span>
        <span className="small-text-label hours">Hours</span>
        <span className="check-flag-label"> {props.item.Title}</span>
        <span className="hours-box" style={style}>
          {props.item.Hours}
        </span>
      </div>
    </div>
  );
}

export default Item;
