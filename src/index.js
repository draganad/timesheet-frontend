import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./index.css";
import App from "./App";

const AppRouter = props => (
  <Router>
    <Switch>
      <Route path="/" exact render={() => <App date={null} />} />
      <Route
        path="/:date"
        render={props => <App date={props.match.params.date} />}
      />
    </Switch>
  </Router>
);

ReactDOM.render(<AppRouter />, document.getElementById("root"));
