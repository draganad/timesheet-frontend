import React, { Component } from "react";
import "./App.css";
import Quote from "./Quote";
import NewTask from "./NewTask";
import Item from "./Item";
import moment from "moment";

class App extends Component {
  state = {
    modalShown: false,
    items: [],
    date: this.props.date ? moment(this.props.date, "DD-MM-YYYY") : moment()
  };

  componentDidMount() {
    fetch(
      "http://localhost:62850/api/entries?date=" +
        this.state.date.format("YYYY-MM-DD")
    )
      .then(res => res.json())
      .then(items => {
        this.setState({
          items
        });
      });
  }

  showModalHandler() {
    this.setState({ modalShown: true });
  }

  hideModalHandler() {
    if (this.state.modalShown) {
      this.setState({ modalShown: false });
    }
  }

  addItemHandler(item) {
    this.state.items.push(item);
  }

  render() {
    const reducer = (acc, cur) => acc + cur.Hours;

    let totalHours = this.state.items.reduce(reducer, 0);

    return (
      <div className="page-wrap">
        <header className="header">
          <div className="wrap">
            <span className="btn-icon">
              <img
                className="icon icon-plus js-modal-init"
                src="icons/icon-plus.svg"
                alt="Add New Item"
                onClick={this.showModalHandler.bind(this)}
              />
            </span>
            <NewTask
              modalShown={this.state.modalShown}
              onHideModal={this.hideModalHandler.bind(this)}
              onAddItem={this.addItemHandler.bind(this)}
              date={this.state.date}
            />
            <Quote />
          </div>
          <div className="header-inner">
            <div className="wrap">
              <div className="date-wrap">
                <img
                  className="icon"
                  src="icons/icon-calendar.svg"
                  alt="Calendar"
                />
                <time>{this.state.date.format("DD/MM/YYYY")}</time>
              </div>
            </div>
          </div>
        </header>
        <main className="main">
          <div className="wrap">
            {this.state.items.map(item => (
              <Item item={item} key={item.Id} />
            ))}
            <div className="total align-right">
              <label htmlFor="" className="total-label">
                Total:
              </label>
              <input
                className="total-input"
                type="text"
                value={totalHours}
                readOnly
              />
            </div>
          </div>
        </main>
        <footer className="footer">
          <div className="wrap"></div>
        </footer>
      </div>
    );
  }
}

export default App;
