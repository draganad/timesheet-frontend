import React from "react";
import quotesJson from "./quotes.json";

function Quote() {
  const len = quotesJson.quotes.length;

  const index = Math.floor(Math.random() * len);

  const quote = quotesJson.quotes[index];

  return (
    <div className="header-blockquote">
      <h1 className="header-quote">{quote.quote}</h1>
      <div className="header-cite">- {quote.author}</div>
    </div>
  );
}

export default Quote;
