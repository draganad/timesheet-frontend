import React, { Component } from "react";

class NewTask extends Component {
  constructor(props) {
    super(props);

    this.state = { Title: "", Hours: "", ErrorMessage: "" };
  }

  handleChange = event => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({ [nam]: val });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ ErrorMessage: "" });

    const data = {
      Title: this.state.Title,
      Hours: parseInt(this.state.Hours),
      Date: this.props.date.format("YYYY-MM-DD")
    };
    const item = { Title: this.state.Title, Hours: parseInt(this.state.Hours) };

    fetch("http://localhost:62850/api/entries", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(response => {
      if (response.ok) {
        this.props.onAddItem(item);
        this.props.onHideModal();
      } else {
        this.setState({ ErrorMessage: "Exceded maximum hours per day" });
      }
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.modalShown !== prevProps.modalShown) {
      this.setState({ Title: "", Hours: "", ErrorMessage: "" });
    }
  }

  render() {
    let classes = "modal-wrap js-modal ";

    classes += this.props.modalShown ? "is-visible" : "";

    let disabled =
      this.state.Title === "" || this.state.Hours === "" ? "disabled" : "";

    return (
      <div className={classes} onClick={this.props.onHideModal}>
        <div
          className="modal js-modal-inner"
          onClick={e => e.stopPropagation()}
        >
          <h2>Create a task:</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="field-wrap">
              <label className="label" htmlFor="">
                Title:
              </label>
              <input
                className="field"
                type="text"
                name="Title"
                placeholder="Enter Title here..."
                value={this.state.Title}
                onChange={this.handleChange}
              />
            </div>
            <div className="field-wrap">
              <label className="label" htmlFor="">
                Hours:
              </label>
              <input
                className="field"
                type="text"
                name="Hours"
                placeholder="Add Hours here..."
                value={this.state.Hours}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <div className="btn-wrap align-left">
                <span id="error">{this.state.ErrorMessage}</span>
              </div>
              <div className="btn-wrap align-right">
                <input
                  className="btn"
                  type="submit"
                  value="Create"
                  disabled={disabled}
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default NewTask;
